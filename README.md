# Division 2 Calculator

An application for tracking Experience Point (XP) gains in the game Tom Clancy's The Division 2. Written in Python.

## CI/CD

### Runner Dependencies
* Python
* pyenv-win
* poetry
* pyinstaller
* GitLab's release-cli

### Starting the Runner
* Open the Windows Command Prompt and navigate to `C:\Users\schism\CI\gitlab`
* Check whether a local runner is registered with this project
  * If not, go to Settings > CI/CD > Runners > New Project Runner
  * Enable `Run untagged jobs` and click `Create Runner`
  * Select Windows and run the runner registration command in CMD
  * Select the `shell` executor
  * In the GitLab UI, go back to the list of available runners for the project and disable the Instance runners
* Run `gitlab-runner.exe run --working-directory build-area`
* If using a existing runner config, delete the config block(s) from any previous runners
* In the config block rename the `shell` attribute from `pwsh` to `powershell`
* Restart the runner
* Run a pipeline to validate operation