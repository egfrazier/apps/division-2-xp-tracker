APP_ROOT=xp_tracker
TEST_ROOT=tests

lint:
			poetry run mypy --strict $(APP_ROOT) && poetry run mypy --strict $(TEST_ROOT)
			poetry run flake8 $(APP_ROOT) && poetry run flake8 $(TEST_ROOT)

fmt:
			black $(APP_ROOT) && black $(TEST_ROOT)

test:
			poetry run pytest $(TEST_ROOT)

build:
			poetry run pyinstaller --onefile $(APP_ROOT)\main.py