def main() -> str:
    you = input("Who are you? ")

    greeting = f"Hello, {you}!"
    print(greeting)
    return greeting


if __name__ == "__main__":
    main()
