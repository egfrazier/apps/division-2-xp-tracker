import pytest

from xp_tracker.main import main

YOU = "Pooh"


def test_main(monkeypatch: pytest.MonkeyPatch) -> None:
    monkeypatch.setattr("builtins.input", lambda _: YOU)

    result = main()
    assert result == f"Hello, {YOU}!"
